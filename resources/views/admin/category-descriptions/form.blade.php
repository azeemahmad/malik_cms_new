<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>CategoryDescription</b></div>
        <br/>

        <div class="form-group {{ $errors->has('descriptions') ? 'has-error' : ''}}">
            <label for="descriptions" class="col-md-2 control-label">{{ 'Descriptions' }} :</label>

            <div class="col-md-8">
                <textarea class="form-control" rows="5" name="descriptions" type="textarea" id="descriptions"
                          required>{{ isset($categorydescription->descriptions) ? $categorydescription->descriptions : ''}}</textarea>
                {!! $errors->first('descriptions', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
            <label for="category_id" class="col-md-2 control-label">{{ 'Category Id' }} :</label>

            <div class="col-md-8">
                <select name="category_id" class="form-control" id="category_id" required>
                    @foreach(App\Models\Project::where('works','!=',null)->groupBy('works')->orderBy('name','ASC')->get() as $key => $value)
                        <option value="{{ $value->id }}" {{ (isset($categorydescription->project_id) && $categorydescription->project_id == $value->id) ? 'selected' : ''}}>{{ ucwords($value->works) }}</option>
                    @endforeach
                </select>
                {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('project_id') ? 'has-error' : ''}}">
            <label for="project_id" class="col-md-2 control-label">{{ 'Project Id' }} :</label>

            <div class="col-md-8">
                <select name="project_id" class="form-control" id="project_id" required>
                    @foreach(App\Models\Project::orderBy('name','ASC')->get() as $key => $value)
                        <option value="{{ $value->id }}" {{ (isset($categorydescription->project_id) && $categorydescription->project_id == $value->id) ? 'selected' : ''}}>{{ $value->name }}</option>
                    @endforeach
                </select>
                {!! $errors->first('project_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-4 col-md-6">
                <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
            </div>
        </div>
    </div>
</div>
