@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-success">
                    <div class="panel-heading">ProjectImage {{ $projectimage->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/project-image') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/project-image/' . $projectimage->id . '/edit') }}"
                           title="Edit ProjectImage">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/projectimage' . '/' . $projectimage->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete ProjectImage"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ isset($projectimage->project->name)?$projectimage->project->name:'' }}</td>
                                </tr>
                                <tr>
                                    <th> Project Name</th>
                                    <td><a href="{{asset('ProjectImages/'.$projectimage->filename)}}" target="_blank"><img src="{{asset('ProjectImages/'.$projectimage->filename)}}"  height="120" width="120"></a></td>
                                </tr>
                                <tr>
                                    <th> Filename</th>
                                    <td> {{ $projectimage->filename }} </td>
                                </tr>
                                <tr>
                                    <th> Thumbnails</th>
                                    <td> {{ $projectimage->thumbnails }} </td>
                                </tr>
                                <tr>
                                    <th> Category</th>
                                    <td> {{ ucwords($projectimage->category_id) }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
