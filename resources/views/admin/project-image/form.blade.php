<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>ProjectImage</b></div>
        <br/>
         <div class="form-group {{ $errors->has('project_id') ? 'has-error' : ''}}">
    <label for="project_id" class="col-md-2 control-label">{{ 'Project Id' }} :</label>
    <div class="col-md-8">
     <select name="project_id" class="form-control" id="project_id" required>
         @foreach(App\Models\Project::orderBy('name','ASC')->get() as $key => $value)
             <option value="{{ $value->id }}" {{ (isset($projectimage->project_id) && $projectimage->project_id == $value->id) ? 'selected' : ''}}>{{ $value->name }}</option>
         @endforeach

    </select>
    {!! $errors->first('project_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('filename') ? 'has-error' : ''}}">
    <label for="filename" class="col-md-2 control-label">{{ 'Filename' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="filename" type="file" id="filename" value="{{ isset($projectimage->filename) ? $projectimage->filename : ''}}" required>
    {!! $errors->first('filename', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('thumbnails') ? 'has-error' : ''}}">
    <label for="thumbnails" class="col-md-2 control-label">{{ 'Thumbnails' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="thumbnails" type="text" id="thumbnails" value="{{ isset($projectimage->thumbnails) ? $projectimage->thumbnails : ''}}" >
    {!! $errors->first('thumbnails', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('category_id') ? 'has-error' : ''}}">
    <label for="category_id" class="col-md-2 control-label">{{ 'Category Id' }} :</label>
    <div class="col-md-8">
     <select name="category_id" class="form-control" id="category_id" required>
    @foreach (json_decode('{"educational": "Educational", "commercial": "Commercial","hospitality":"Hospitality","research":"Research","residential":"Residential","healthcare":"Healthcare"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($projectimage->category_id) && $projectimage->category_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('category_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
          <div class="form-group">
                <div class="col-md-offset-4 col-md-6">
                      <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
                </div>
          </div>
     </div>
</div>
