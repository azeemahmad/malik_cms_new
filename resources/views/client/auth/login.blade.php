<!DOCTYPE html>
<html>
<head>
    <title>Rolebased</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- stylesheet	 -->
    <link rel="stylesheet" type="text/css" href="{{asset('css/video/bootstrap.min.css')}}">

    <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>

    <link rel="stylesheet" type="text/css" href="{{asset('css/video/mystyle.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.1/css/all.css" integrity="sha384-O8whS3fhG2OnA5Kas0Y9l3cfpmYjapjI0E4theH4iuMD+pLhbf6JI0jIMfYcK3yZ" crossorigin="anonymous">
    <!--<link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css">-->
    <!-- Animate CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.css" />
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/one-page-wonder.min.css')}}" rel="stylesheet">

    <!-- End  of stylesheet -->
    <style type="text/css">
        body{
            background: transparent;
        }
        .login-page header{
            border-bottom: 4px solid #f37121;
            box-shadow: 1px 1px 10px 1px grey;
            padding: 5px;
            /*background: #d2d2d2;*/
            margin: 0px;
        }
        .login-page #login .container #login-row #login-column #login-box {
            margin-top: 15px;
            max-width: 600px;
            /*height: 320px;*/
            padding-top: 15px;
            border: 1px solid #9C9C9C;
            background-color: transparent;
            border-radius: 10px;
        }
        .info-tooltip{
            border: 0px;
            background: transparent;
        }
        .imagesize{
            width: 20px;
        }
        .info-tooltip{
            border: 0px;
            background: transparent;
        }
        #info-modal .modal-dialog {
            left: 0px;
        }
        .info-modal .close-modal{
            margin-top: -2px;
            font-size: 30px;
            float: right;
            font-size: 21px;
            font-weight: bold;
            line-height: 1;
            color: #000;
            text-shadow: 0 1px 0 #fff;
            opacity: 1;
            border: 0px;
            background: transparent;
            font-size: 25px;
            outline: none;
        }
        .info-modal {
            z-index: 999999;
        }
        .form-group.form-check.accept-checkbox {
            margin-bottom: 5px;
            font-size: 12px;
        }
        .info-modal-outer .info-modal-click{
            font-size: 12px;
        }
        .info-modal-outer .info-modal-click img, .info-tooltip img{
            width: 15px;
            margin-left: 5px;
        }
        .info-modal h5{
            font-weight: 600;
        }
        .info-modal {
            font-size: 13px;
        }
        .require:after{
            content:'*';
            color:red;
        }
        .login-page #login .container #login-row #login-column #login-box #login-form {
            padding: 5px 20px;
            border-bottom: 1px solid gainsboro;
        }
        .login-page #login .container #login-row #login-column #login-box #login-form #register-link {
            margin-top: -45px;
            text-align: right;

        }
        .login-page section#login  {
            /*height: 100vh;*/
            padding-bottom: 20px;
        }
        .login-page h3.text-center.text-white.pt-5 {
            padding-top: 120px;
            margin: 0px;
        }
        .login-page .forgot-password{
            line-height: 3;
        }
        .login-page .form-control{
            background: transparent;
        }
        .login-page .text-info {
            color: #5d5d5d;
        }
        input.btn.submit-btn.btn-info.btn-md {
            width: 150px;
            padding: 10px;
            background: #f5700c;
            border: none;
            margin-top: 20px;
        }
        .help-block{
            color: red;
            font-size: larger;
        }
        .require:after{
            content:'*';
            color:red;
        }
        #footer{
            bottom: 0;
            /*position: fixed;*/
            width: 100%;
        }
        .login-page div#login-column {
            height: 75vh;
        }
        .margin-bottom-0{
            margin-bottom: 0px;
        }
        .omb_login .omb_authTitle {
            text-align: center;
            line-height: 200%;
            margin-top: 10px;
            margin-bottom: 5px;
        }

        .omb_socialButtons {
            padding-bottom: 20px;
        }
        .headermain{
            height: 83px;
        }

        @media(max-width:768px){
            nav.main_navigation, footer {
                display: none;
            }
        }
    </style>

</head>
<body class="login-page">

<header id="header" class="headermain">
    <div class="logo">
        <a class="navigation_link" href="{{url('/')}}"><img src="{{asset('role_based_image.png')}}" alt=""
                                                            class="white_logo"></a>
        <a class="navigation_link" href="{{url('/')}}"> <img src="{{asset('role_based_image.png')}}" alt=""
                                                             class="orange_logo"></a>
    </div>


</header><!-- /header -->

<section class="section" id="login">

    <h3 class="text-center text-white pt-5">Login</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6 col-md-offset-3">
                <div id="login-box" class="col-md-12">
                    @if ($message = Session::get('error_message'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            {{ $message }}
                        </div>
                    @endif

                    <form class="form" id="login-form" role="form" method="POST" action="{{ url('/client/login') }}">
                        {{ csrf_field() }}
                        {{--<h3 class="text-center text-info">Login</h3>--}}
                        <div class="form-group">
                            <label for="username" class="text-info require">Username: </label><br>
                            <input type="text" name="email" id="username" class="form-control" value="{{old("email")}}">
                            @if ($errors->has('email'))
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            @endif
                        </div>
                        <div class="form-group margin-bottom-0">
                            <label for="password" class="text-info require">Password: </label><br>
                            <input type="password" name="password" id="password" class="form-control">
                            @if ($errors->has('password'))
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            @endif
                        </div>
                        <div class="form-group"><br>
                            <label for="password" class="text-info">Remember me: </label>&nbsp;&nbsp;
                            <input checked="checked" name="remember" type="checkbox" id="remember">
                        </div>
                        <div class="form-group margin-bottom-0">
                            <input type="submit"  class="btn submit-btn btn-info btn-md" value="Login">
                        </div>
                        <div class="form-group">
                            <!-- <input type="checkbox" name="vehicle1" value="Bike"> I have a bike<br>
                            <label for="remember-me" class="text-info"><span>Remember me</span> <span><input id="remember-me" name="remember-me" type="checkbox"></span></label><br> -->
                            <a href="{{ url('client/password/reset') }}" class="text-info forgot-password">Forgot password</a>
                        </div>
                        <div id="register-link" class=" form-group">
                            <a href="{{ url('/client/register') }}" class="text-info">Register here</a>
                        </div>
                    </form>
                    <div class="omb_login">
                        <h3 class="omb_authTitle">Or Login With</h3>

                        <div class="row omb_row-sm-offset-3 omb_socialButtons">
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-facebook">
                                    <i class="fa fa-facebook visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-facebook"></span></span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-twitter">
                                    <i class="fa fa-twitter visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-twitter"></span></span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-google">
                                    <i class="fa fa-google-plus visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-google-plus"></span></span>
                                </a>
                            </div>
                            <div class="col-xs-4 col-sm-2">
                                <a href="#" class="btn btn-md btn-block omb_btn-linkedin">
                                    <i class="fa fa-linkedin visible-xs"></i>
                                    <span class="hidden-xs"><span class="fa fa-linkedin"></span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer id="footer" class="" >
    <div class="stay_updated">
    </div>
    <div class="footer_block">
        <div class="container">
            <div class="col-md-7">
                <div class="footer_logo col-md-4">
                    <img src="{{asset('role_based_image.png')}}" alt="">
                </div>
                <div class="copyright col-md-8">
                    <p>
                        Copyright @ 2019 Role Based
                    </p>
                </div>
            </div>
            <div class="col-md-5 text-right">
            </div>
        </div>
    </div>
</footer>
</body>
</html>


