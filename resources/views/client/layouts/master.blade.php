<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{{ asset('faviconR.ico') }}">
    <title>{{ isset($page_title)?$page_title:"Malik CMS" }}</title>
    <link rel="stylesheet" type="text/css" href="css/feature-carousel.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- Owl carousel -->
    <link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
    <link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
    <link rel="stylesheet" type="text/css" href="css/owl.theme.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- custom styles -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <link href="css/ilightbox.css" rel="stylesheet">
    @yield('css')
</head>
<body>
    @include('client.layouts.header')
    @yield('content')
    @include('client.layouts.footer')
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src='js/owl.carousel.min.js'></script>
    <script src='js/jquery.mixitup.min.js'></script>
    <script src='js/ilightbox.packed.js'></script>
    <script src="js/main.js"></script>
    <script src="js/jquery.requestAnimationFrame.js"></script>
    <script src="js/jquery.mousewheel.js"></script>
@yield('scripts')
</body>
</html>