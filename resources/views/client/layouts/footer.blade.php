<section class="inner_section" id="contact_section">
    <div class="container">
        <div class="col-md-4">
            <h1 class="tittle">Feel free to call us or <br> Visit Us</h1>
            <span class="divider"></span>
        </div>
        <div class="col-md-3 address_block">
            <h3>Head Office</h3>
            <p>
                Malik Architecture<br>2nd Floor, Kaiser-I-Hind,<br>1/6 Currimbhoy Road,<br>Ballard Estate, Mumbai - 400001.<br>(Near Grand Hotel, Opp. Cafe Model)
            </p>
            <p>
                Ph: 022-22642170/71/72/74
            </p>
            <p>Fax: 022 – 22620345</p>
        </div>
        <div class="col-md-5">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3773.9000803356316!2d72.83807111546015!3d18.93581638716855!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7cf21727d5445%3A0xbf4a8a605d9a1c40!2sMalik+Architecture!5e0!3m2!1sen!2sin!4v1535719430164" width="100%" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</section>

<footer id="footer" class="">
    <div class="container">
        <div class="col-md-8">
            <p>
                copyright 2018 malik architecture, all right reserved. powered by <a href="http://firsteconomy.com/" target="_blank">firsteconomy.com</a>
            </p>
        </div>
        <div class="col-md-4">
            <ul class="footer_social">
                <li>
                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                    <a href="#"><i class="fab fa-linkedin-in"></i></a>
                </li>
            </ul>
        </div>
    </div>
</footer>