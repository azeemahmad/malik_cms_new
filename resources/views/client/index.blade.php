@extends('client.layouts.master')
@section('content')
    <section class="home_banner">
        <div class="wrapper">
            <ul class="owl-carousel" id="home_slider">
                <li><img src="images/slider/slider1.jpg" alt=""></li>
                <li><img src="images/slider/slider2.jpg" alt=""></li>
                <li><img src="images/slider/slider3.jpg" alt=""></li>
                <li><img src="images/slider/slider4.jpg" alt=""></li>
                <li><img src="images/slider/slider5.jpg" alt=""></li>
                <li><img src="images/slider/slider6.jpg" alt=""></li>
                <li><img src="images/slider/slider7.jpg" alt=""></li>
            </ul>
        </div>
    </section>

    <section class="inner_section" id="quote_section">
        <div class="container">
            <div class="col-md-12 text-center quote_box">
                <h3>
                    <i>"The mind is everything. What you think you become."</i>
                </h3>
                <h5>"Gautam Buddha"</h5>
            </div>
        </div>
    </section>
    <section class="inner_section" id="services">
        <div class="container-fluid service_inner">
            <div class="container">
                <div class="col-md-6">
                    <h1 class="tittle">we are the best architect firm <br>& building consultant</h1>
                    <span class="divider"></span>
                </div>
                <div class="col-md-6">
                    <p>
                        Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Book
                    </p>
                    <p>
                        Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Book. Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Book
                    </p>
                </div>
            </div>
        </div>
        <div class="container-fluid flip_blocks">
            <div class="container">
                <div class="col-md-4">
                    <div class="service_block_1">
                        <img src="images/service_icons/1.png" alt="">
                        <h3>Architecture</h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service_block_1">
                        <img src="images/service_icons/2.png" alt="">
                        <h3>Planning</h3>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="service_block_1">
                        <img src="images/service_icons/3.png" alt="">
                        <h3>Interior Design</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid flip_block_text">
            <div class="container">
                <div class="col-md-4">
                    <h3>We pride ourselves on innovative.</h3>
                    <p>
                        Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Seman tics, a large language ocean. Pointing has no control about the blind.
                    </p>
                    <!-- <a href="#" class="learn_more">Learn More <i class="fas fa-long-arrow-alt-right"></i></a> -->
                </div>
                <div class="col-md-4">
                    <h3>We pride ourselves on innovative.</h3>
                    <p>
                        Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Seman tics, a large language ocean. Pointing has no control about the blind.
                    </p>
                </div>
                <div class="col-md-4">
                    <h3>We pride ourselves on innovative.</h3>
                    <p>
                        Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Seman tics, a large language ocean. Pointing has no control about the blind.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="inner_section" id="featured_work">
        <div class="container">
            <div class="col-md-6">
                <h1 class="tittle">Featured Works</h1>
                <span class="divider"></span>
            </div>
        </div>
        <div class="container">
            <!-- <div class="controls">
              <button class="filter" data-filter="all">All</button>
              <button class="filter" data-filter=".category-1">Category 1</button>
              <button class="filter" data-filter=".category-2">Category 2</button>
            </div> -->
            <div id="projects">
                <div class="mix category-1" data-myorder="1">
                    <a href="project_inner.php">
                        <img src="images/featured_work/1.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-1" data-myorder="2">
                    <a href="#">
                        <img src="images/featured_work/2.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-1" data-myorder="3">
                    <a href="#">
                        <img src="images/featured_work/3.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-2" data-myorder="4">
                    <a href="#">
                        <img src="images/featured_work/4.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-1" data-myorder="5">
                    <a href="#">
                        <img src="images/featured_work/5.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-1" data-myorder="6">
                    <a href="#">
                        <img src="images/featured_work/6.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-2" data-myorder="7">
                    <a href="#">
                        <img src="images/featured_work/7.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-2" data-myorder="8">
                    <a href="#">
                        <img src="images/featured_work/8.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-2" data-myorder="7">
                    <a href="#">
                        <img src="images/featured_work/9.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
                <div class="mix category-2" data-myorder="8">
                    <a href="#">
                        <img src="images/featured_work/10.jpg" alt="">
                        <div class="overlay_cont">
                            <h4>GMS Grand Palladium</h4>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
@endsection