<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ProjectImage;
use Illuminate\Http\Request;
//use App\Authorizable;

class ProjectImageController extends Controller
{
   //  use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $projectimage = ProjectImage::where('project_id', 'LIKE', "%$keyword%")
                ->orWhere('filename', 'LIKE', "%$keyword%")
                ->orWhere('thumbnails', 'LIKE', "%$keyword%")
                ->orWhere('category_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $projectimage = ProjectImage::latest()->paginate($perPage);
        }

        return view('admin.project-image.index', compact('projectimage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.project-image.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'project_id' => 'required',
			'filename' => 'required',
			'category_id' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('filename')) {
        $filename = $this->getFileName($request->filename);
                    $request->filename->move(base_path('public/ProjectImages'), $filename);
                    $requestData['filename'] = $filename;
        }

        ProjectImage::create($requestData);

        return redirect('admin/project-image')->with('flash_message', 'ProjectImage added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $projectimage = ProjectImage::findOrFail($id);

        return view('admin.project-image.show', compact('projectimage'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $projectimage = ProjectImage::findOrFail($id);

        return view('admin.project-image.edit', compact('projectimage'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'project_id' => 'required',
			'filename' => 'required',
			'category_id' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('filename')) {
            $filename = $this->getFileName($request->filename);
            $request->filename->move(base_path('public/ProjectImages'), $filename);
            $requestData['filename'] = $filename;
        }

        $projectimage = ProjectImage::findOrFail($id);
        $projectimage->update($requestData);

        return redirect('admin/project-image')->with('flash_message', 'ProjectImage updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ProjectImage::destroy($id);

        return redirect('admin/project-image')->with('flash_message', 'ProjectImage deleted!');
    }
    protected function getFileName($file)
    {
        return str_random(16) . '.' . $file->extension();
    }
}
