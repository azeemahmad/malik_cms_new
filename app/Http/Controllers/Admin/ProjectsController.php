<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\ProjectImage;
use Illuminate\Http\Request;
use App\Authorizable;
class ProjectsController extends Controller
{
      use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $projects = Project::where('category', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('project_description', 'LIKE', "%$keyword%")
                ->orWhere('banner', 'LIKE', "%$keyword%")
                ->orWhere('images', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $projects = Project::latest()->paginate($perPage);
        }

        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'category' => 'required',
            'works' => 'required',
			'name' => 'required|unique:projects',
			'project_description' => 'required',
		]);
        $requestData = $request->all();
         if ($request->hasFile('banner')) {
            $filename = $this->getFileName($request->banner);
            $request->banner->move(base_path('public/ProjectBanner'), $filename);
            $requestData['banner'] = $filename;
        }
        $requestData['slug'] = strtolower(str_replace(' ','-',$requestData['name']));
        $project= Project::create($requestData);
         if ($files =$request->hasFile('images')) {
            foreach ($request->images as $file) {
                $fileImagename = $this->getFileName($file);
                $file->move(base_path('public/ProjectImages'), $fileImagename);
                $ImageData['filename'] = $fileImagename;
                $ImageData['thumbnails'] = 'todo';
                $ImageData['category_id'] = $requestData['category'];
                $ImageData['project_id'] = $project->id;
                ProjectImage::create($ImageData);
            }

        }

        return redirect('admin/projects')->with('flash_message', 'Project added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $project = Project::findOrFail($id);

        return view('admin.projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);

        return view('admin.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'category' => 'required',
            'works' => 'required',
            'name' => 'unique:projects,name,' . $id,
			'project_description' => 'required',
		]);
        $requestData = $request->all();
        if ($request->hasFile('banner')) {
            $filename = $this->getFileName($request->banner);
            $request->banner->move(base_path('public/ProjectBanner'), $filename);
            $requestData['banner'] = $filename;

        }
        $requestData['slug'] = strtolower(str_replace(' ','-',$requestData['name']));
        $project = Project::findOrFail($id);
        $project->update($requestData);
        if ($files =$request->hasFile('images')) {
            foreach ($request->images as $file) {
                $fileImagename = $this->getFileName($file);
                $file->move(base_path('public/ProjectImages'), $fileImagename);
                $ImageData['filename'] = $fileImagename;
                $ImageData['thumbnails'] = 'todo';
                $ImageData['category_id'] = $requestData['category'];
                $ImageData['project_id'] = $project->id;
                ProjectImage::create($ImageData);
            }

        }

        return redirect('admin/projects')->with('flash_message', 'Project updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $project = Project::findOrFail($id);
        $project->projectimages()->delete();
        $project->delete();
      return redirect('admin/projects')->with('flash_message', 'Project deleted!');
    }

    protected function getFileName($file)
    {
        return str_random(16) . '.' . $file->extension();
    }
}
