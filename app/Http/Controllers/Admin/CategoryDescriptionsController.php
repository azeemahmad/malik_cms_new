<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CategoryDescription;
use Illuminate\Http\Request;
use App\Authorizable;

class CategoryDescriptionsController extends Controller
{
     // use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $categorydescriptions = CategoryDescription::where('descriptions', 'LIKE', "%$keyword%")
                ->orWhere('project_id', 'LIKE', "%$keyword%")
                ->orWhere('category_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $categorydescriptions = CategoryDescription::latest()->paginate($perPage);
        }

        return view('admin.category-descriptions.index', compact('categorydescriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.category-descriptions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'project_id' => 'required',
			'descriptions' => 'required',
			'category_id' => 'required'
		]);
        $requestData = $request->all();
        
        CategoryDescription::create($requestData);

        return redirect('admin/category-descriptions')->with('flash_message', 'CategoryDescription added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $categorydescription = CategoryDescription::findOrFail($id);

        return view('admin.category-descriptions.show', compact('categorydescription'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $categorydescription = CategoryDescription::findOrFail($id);

        return view('admin.category-descriptions.edit', compact('categorydescription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'project_id' => 'required',
			'descriptions' => 'required',
			'category_id' => 'required'
		]);
        $requestData = $request->all();
        
        $categorydescription = CategoryDescription::findOrFail($id);
        $categorydescription->update($requestData);

        return redirect('admin/category-descriptions')->with('flash_message', 'CategoryDescription updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        CategoryDescription::destroy($id);

        return redirect('admin/category-descriptions')->with('flash_message', 'CategoryDescription deleted!');
    }
}
