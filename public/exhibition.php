<!DOCTYPE html>
<html>
<head>
    <title>Exhibition | Malik</title>
    
    <?php include_once('includes/header.php'); ?>

    <section class="">
		<div class="">
			<img src="images/project_inner_banner.jpg" alt="">
		</div>
	</section>

	<section class="inner_section section_exhi">
		<div class="container">
			<div class="row">

				<div class="col-md-6 left_box">
					<div class="exhi_image">
						<img src="images/project_inner/large/2.jpg">
					</div>
					<div class="exhi_body">
						<h3>Exhibition name</h3>
						<div class="exhi_date">
							<span class="fa fa-calendar-alt"></span>25 September - 27 September 2018
						</div>

						<div  class="exhi_loc">
							<span class="fa fa-map-marker-alt"></span>location
						</div>

						<div class="exhi_details">
							<p><strong>Far far away</strong>, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
						</div>
					</div>
					<div class="exhi_footer text-right">
						<span>In Partnership with</span><img src="images/logo.png">
					</div>
				</div>


				<div class="col-md-6 right_box">
					<div class="col-md-6">
						<div class="exhi_image">
							<img src="images/project_inner/large/2.jpg">
						</div>
						<div class="exhi_body">
							<h3>Exhibition name</h3>
							<div class="exhi_date">
								<span class="fa fa-calendar-alt"></span>25 September - 27 September 2018
							</div>

							<div  class="exhi_loc">
								<span class="fa fa-map-marker-alt"></span>location
							</div>

							<div class="exhi_details text-justify">
								<p><strong>Far far away, behind the word mountains</strong>, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							</div>
						</div>
						<div class="exhi_footer text-right">
							<span>In Partnership with</span><img src="images/logo.png">
						</div>
					</div>
					<div class="col-md-6">
						<div class="exhi_image">
							<img src="images/project_inner/large/2.jpg">
						</div>
						<div class="exhi_body">
							<h3>Exhibition name</h3>
							<div class="exhi_date">
								<span class="fa fa-calendar-alt"></span>25 September - 27 September 2018
							</div>

							<div  class="exhi_loc">
								<span class="fa fa-map-marker-alt"></span>location
							</div>

							<div class="exhi_details">
								<p><strong>Far far away, behind </strong>the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
							</div>
						</div>
						<div class="exhi_footer text-right">
							<span>In Partnership with</span><img src="images/logo.png">
						</div>
					</div>
				</div>

			</div>	


		</div>
	</section>
    
   
    <?php include_once('includes/footer.php'); ?>
    
</body>
</html>







