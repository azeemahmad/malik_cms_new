<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- stylesheets -->
		<link rel="stylesheet" type="text/css" href="css/feature-carousel.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">

	<!-- Owl carousel -->
	<link rel="stylesheet" type="text/css" href="css/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="css/owl.transitions.css">
	<link rel="stylesheet" type="text/css" href="css/owl.theme.css">
	<!-- fontawesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
	<!-- custom styles -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/responsive.css">
	<link href="css/ilightbox.css" rel="stylesheet">

</head>
<body>

	<div id="preloader">
		<div id="status">
			<div class="preloader_logo">
				<img src="images/logo.jpg" alt="">
			</div>
			<div class="spinners"><em></em><em></em><em></em><em></em></div>
		</div>
	</div>

	<header id="header" class="">
		<div class="container">
			<div class="logo">
				<a href="index.php"><img src="images/logo.png" alt=""></a><br>
				<div class="menu_icon">
					<span class="icon_bar"></span>
					<span class="icon_bar"></span>
					<span class="icon_bar"></span>
				</div>
			</div>
			<div class="navbar">
				<div class="close_btn"><img src="images/icons/close.png" alt=""></div>
				<ul class="nav_menu">
					<li><a href="index.php">Home</a></li>
					<li class="dropdown" id="work">
						<a href="javascript:void(0);">Work <i class="fas fa-chevron-right"></i></a>
						<ul class="submenu sub_work">
							<li><a href="architecture.php">Architecture</a></li>
							<li><a href="interior.php">Interior Design</a></li>
							<li><a href="planning.php">Planning</a></li>
						</ul>
					</li>
					<li><a href="#">Philosophy</a></li>
					<li class="dropdown" id="practice">
						<a href="javascript:void(0);">The Practice <i class="fas fa-chevron-right"></i></a>
						<ul class="submenu sub_practice">
							<li><a href="team.php">The Team</a></li>
							<li><a href="#">Competitions</a></li>
							<li><a href="awards.php">Awards</a></li>
							<li><a href="media.php">Media</a></li>
							<li><a href="exhibition.php">Exhibitions?</a></li>
							<li><a href="career.php">Careers</a></li>
						</ul>
					</li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</div>
		</div>
	</header><!-- /header -->