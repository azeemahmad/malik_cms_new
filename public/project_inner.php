
  <?php session_start();

  include('includes/project_inner_config.php');


  if( isset($_GET['idk']) && !empty($_GET['idk']))
  {
  $slug= urldecode($_REQUEST['idk']);

  $sql_projects = "SELECT p.*,pi.* FROM `projects` p INNER JOIN `project_images` pi on (`pi`.`project_id`=`p`.`id`) where `p`.`slug` = '".$slug."'";

  $result_projects = mysqli_query($db,$sql_projects);

  while($row_projects = mysqli_fetch_array($result_projects)){

    $row_project[] = $row_projects;


  }

}
else {
  header('Location: architecture.php');
}
  $p_id=$row_project[0][0];
  $projectdescription="SELECT p.* FROM `category_descriptions` p where `p`.`project_id` = $p_id";
  $result_projects_descriptions = mysqli_query($db,$projectdescription);
  $row_projects_decriptions = mysqli_fetch_array($result_projects_descriptions);

  ?>



<!DOCTYPE html>
<html>
	<title>Malik</title>
	<style>
		.info_block {
			position:  absolute;
			right:  0px;
			top: 50%;
			transform: translate(-50%,-50%);
			/*background: #000;*/
			/* padding: 10px; */
		}

		a.info_btn {
			padding: 3px 25px;
			color: #fff!important;
			background: #000;
			border-radius: 5px;
		}
		button.close {
			background: #000;
			width: 30px;
			color: #fff;
			opacity: 1;
			height: 30px;
			text-align:  center;
			line-height: 30px;
			position:  absolute;
			/* padding: 5px; */
			padding-top: 2px;
			top: 0px;
			right: 0px;
		}

		#info_modal .modal-body{
			padding:30px!important;
		}
	</style>
	<?php include_once('includes/header.php'); ?>

	<section class="inner_banner">
		<img src="<?php echo 'ProjectBanner/'.$row_project[0]['banner'];?>" alt="">
	</section>
	<section class="inner_section">
		<div class="container">

			<div class="col-md-12 breadcrumb">
				<div class="info_block">
					<a href="" class="info_btn" data-toggle="modal" data-target="#info_modal">info</a>
				</div>
				<h1 class="tittle title_inner">
				<a href="architecture.php"><?php echo  ucwords($row_project[0]['works']);?></a> <i class="fas fa-chevron-right"></i> <?php echo $row_project[0]['name'] ?></h1>
			</div>

			<div class="col-md-12">
				<p>
					<?php echo $row_project[0]['project_description'] ?>
				</p>

			</div>
		</div>


		<div class="container">

			<div id="commercial" class="project_inner">

			<ul id="infinite_scroll">
			<?php if(!empty($row_project))
				{
				foreach ($row_project as $key => $value) {
					$large_image = $value['filename'];

                    $width = '192';
                    $height = '192';

                    $thumbnail = preg_replace("|h_800|",'h_192',preg_replace("|w_1200|","w_192",$large_image));

				?>

			  <li class="mix">
			  	<a href="<?php echo 'ProjectImages/'.$value['filename'] ;?>" data-options="thumbnail: 'images/project_inner/thumbnails/<?php echo $thumbnail;?>'">
				  	<img height="200px"  src="<?php echo 'ProjectImages/'.$value['filename'] ;?>" alt="">
			  	</a>
			  </li>

			  <?php }} ?>
			</ul>
		</div>
	</section>


<!--	modal-->
	<div id="info_modal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">

					<h5 class="modal-title"><a href="architecture.php"><?php echo  ucwords($row_project[0]['works']);?></a> <i class="fas fa-chevron-right"></i> <?php echo $row_project[0]['name'] ?></h1></h5>
				</div>
				<div class="modal-body">

					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<p><?php if($row_projects_decriptions){echo $row_projects_decriptions[1];}?></p>
				</div>
			</div>

		</div>
	</div>
<!--	modal end-->
	<?php include_once('includes/footer.php'); ?>
	<script type="text/javascript" src="assets/js/css_browser_selector.min.js"></script>
	<script type="text/javascript" src="http://platform.twitter.com/widgets.js"></script>
    <script src="assets/js/jquery.requestAnimationFrame.js"></script>
	<script type="text/javascript" async defer src="http://iprodev.com/projects.php?p=ilightbox"></script>
    <script src="js/jquery.mousewheel.js"></script>
    <script src="js/ilightbox.packed.js"></script>
	 <script>
	$(document).ready(function(){

	  var $container = $('ul#infinite_scroll'),
	  old = $('li', $container),
	  api = $('ul#infinite_scroll li a').iLightBox(),
	  pages = 3;

	  $container.isotope({
		itemSelector : 'li'
	  });

	  $(document).bind('scroll', function(){
		var t = $(this),
		last = $('#last_divider').offset(),
		scroll = t.scrollTop() + $(window).height();

		if(scroll >= (last.top-200) && pages >= 1) {
			var newElements = old.clone();
			newElements.appendTo($container);
			refresh(newElements);
			pages--;
		}
	  });

	  $(window).bind('resize load', function(){
	    $container.isotope( 'reLayout' );
	  });

	  function refresh(newElements){
		api.refresh();
		$container.isotope( 'appended', $( newElements ) );
	  }


	});

	window.___gcfg = {lang: "en"};
	(function() {
		var po = document.createElement("script"); po.type = "text/javascript"; po.async = true;
		po.src = "https://apis.google.com/js/plusone.js";
		var s = document.getElementsByTagName("script")[0]; s.parentNode.insertBefore(po, s);
	})();
  	</script>
</body>
</html>
