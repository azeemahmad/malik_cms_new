<!DOCTYPE html>
<html>
<head>
    <title>Media | Malik</title>
    
    <?php include_once('includes/header.php'); ?>

    
    <section class="inner_section">
        <div class="container">
            <div class="col-md-4 press_head">
                <h1 class="tittle">Media Reports</h1>
                <span class="divider"></span>
               <!--  <h2>Press Releases</h2> -->
            </div>
            <div class="clearfix"></div>
            <div class="col-md-4 col-sm-6">
                <div class="thumbnail thumbnail_media">
                    <img src="images/media/media.jpg">
                    <div class="caption">
                        <h4>Montreal Tower to become glazed offices for Desjardins Group</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                     <div class="text-center">
                        <a href="#" class="btn_more">More Info</a>
                    </div>
               </div>
            </div>   
             <div class="col-md-4 col-sm-6">
                <div class="thumbnail thumbnail_media">
                    <img src="images/media/media.jpg">
                    <div class="caption">
                        <h4>Montreal Tower to become glazed offices for Desjardins Group</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn_more">More Info</a>
                    </div>
               </div>
            </div>    
             <div class="col-md-4 col-sm-6">
                <div class="thumbnail thumbnail_media">
                    <img src="images/media/media.jpg">
                    <div class="caption">
                        <h4>Montreal Tower to become glazed offices for Desjardins Group</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn_more">More Info</a>
                    </div>
               </div>
            </div>   

              <div class="clearfix"></div>

             <div class="col-md-4 col-sm-6">
                <div class="thumbnail thumbnail_media">
                    <img src="images/media/media.jpg">
                    <div class="caption">
                        <h4>Montreal Tower to become glazed offices for Desjardins Group</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn_more">More Info</a>
                    </div>
               </div>
            </div>   

            <div class="col-md-4 col-sm-6">
                <div class="thumbnail thumbnail_media">
                    <img src="images/media/media.jpg">
                    <div class="caption">
                        <h4>Montreal Tower to become glazed offices for Desjardins Group</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn_more">More Info</a>
                    </div>
               </div>
            </div>   
             <div class="col-md-4 col-sm-6">
                <div class="thumbnail thumbnail_media">
                    <img src="images/media/media.jpg">
                    <div class="caption">
                        <h4>Montreal Tower to become glazed offices for Desjardins Group</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <div class="text-center">
                        <a href="#" class="btn_more">More Info</a>
                    </div>
               </div>
            </div>   

             <div class="clearfix"></div>


        </div>

    </section>

   <!--  <section class="inner_section">
        <div class="container">
            <div class="col-md-4 media_head">
                <h1 class="tittle">Media Reports</h1>
                <span class="divider"></span>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>   
             <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>    
             <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>    
             <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>   

            <div class="clearfix"></div>

             <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>
             <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>    
             <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>    
             <div class="col-md-3 col-sm-6 text-center">
                <div class="thumbnail thumbnail_media">
                    <img src="images/logo.png">
                    <div class="caption">
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                    </div>
                    <a class="btn btn_more">More Info</a>
               </div>
            </div>        

        </div>

    </section> -->

    <?php include_once('includes/footer.php'); ?>
    
</body>
</html>







