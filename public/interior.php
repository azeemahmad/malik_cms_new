<?php session_start();

include('includes/project_inner_config.php');
$sql_projects = "SELECT * FROM `projects` WHERE `works` = 'interior_design'";


$result_projects = mysqli_query($db,$sql_projects);

while($row_projects = mysqli_fetch_array($result_projects)){
	$row_project[] = $row_projects;

}


?>

<!DOCTYPE html>
<html>
	<title>Interior | Malik</title>
	
	<?php include_once('includes/header.php'); ?>

	<section class="inner_banner">
		<img src="images/archi_banner.jpg" alt="">

	</section>

	<section class="inner_section">
		<div class="container">
			<div class="col-md-6">
				<h1 class="tittle">An Overview</h1>
				<span class="divider"></span>
			</div>
			<div class="clearfix"></div>
			<div class="col-md-12">
				<p>
					<?php echo $row_project[0]['project_description'];?>
				</p>
			</div>
		</div>
	</section>

	<section class="inner_section">
		<div class="container">
			<div class="controls">
			  <button class="filter" data-filter="all">All</button>
			  <button class="filter" data-filter=".commercial">Commercial</button>
<!--			  <button class="filter" data-filter=".educational">Educational</button>-->
<!--			  <button class="filter" data-filter=".hospitality">Hospitality</button>-->
<!--			  <button class="filter" data-filter=".residences">Residences</button>-->
<!--			  <button class="filter" data-filter=".r&d">R&D</button>-->
<!--			  <button class="filter" data-filter=".healthcare">Healthcare</button>-->
			  <button class="filter" data-filter=".residential">Residential Homes</button>
			</div>
			<div id="projects_inner">
				<?php if(!empty($row_project))
				{
					foreach ($row_project as $key => $value) {

						$sql_thumbs = "SELECT `project_images`.filename FROM `projects` INNER JOIN `project_images`
           ON `projects`.id = `project_images`.project_id where `projects`.id = ".$value['id'];
						$result_thumbs = mysqli_query($db,$sql_thumbs);

						while($row_thumbs = mysqli_fetch_row($result_thumbs)){
							$row_thumb = $row_thumbs;
						}
						?>
						<div class="mix <?php echo $value['category'];?>" data-myorder="<?php echo $key;?>">

							<a href="project_inner.php?idk=<?php echo urlencode($value['slug']);?>" >

								<img width="192px" height="192px" src="<?php echo 'ProjectImages/'.$row_thumb[0];?>" alt="">

								<div class="overlay_cont">
									<h4><?php echo $value['name'];?></h4>
								</div>
							</a>
						</div>


					<?php }
				} ?>
			</div>

		</div>
	</section>
	<?php include_once('includes/footer.php'); ?>
	</body>
</html>