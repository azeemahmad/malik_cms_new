<!DOCTYPE html>
<html>
<head>
    <title>Awards | Malik</title>
    
    <?php include_once('includes/header.php'); ?>

    <section class="award_section">
        <div class="container award_container">
            <!-- <img src="images/project_inner_banner.jpg" alt=""> -->
            <div class="col-md-3 awards_header">
                <h1>Awards & Recognitions</h1>
                <span class="devider"></span>
                <span class="devider-line"></span>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
        </div>
     </section>

    <section class="inner_section section_awards">
        <div class="container">
            <div class="col-md-12 text-center awards_slider">
                <h1>Awards</h1>
                <span class="devider"></span>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
            </div>
         
          
  
                <div class="col-md-10 col-md-offset-1 carousel-container">
                
                  <div id="carousel">
                    <div class="carousel-feature">
                      <a href="#"><img class="carousel-image" alt="Image Caption" src="images/awards/5.jpg"></a>
                     </div>
                    <div class="carousel-feature">
                      <a href="#"><img class="carousel-image" alt="Image Caption" src="images/awards/6.jpg"></a>
                   </div>
                    <div class="carousel-feature">
                      <a href="#"><img class="carousel-image" alt="Image Caption" src="images/awards/4.jpg"></a>
                    </div>
                    <div class="carousel-feature">
                      <a href="#"><img class="carousel-image" alt="Image Caption" src="images/awards/5.jpg"></a>
                    </div>
                    <div class="carousel-feature">
                      <a href="#"><img class="carousel-image" alt="Image Caption" src="images/awards/6.jpg"></a>
                     </div>
                  </div>
                
                  <div id="carousel-left"><img src="images/awards/arrow-left.png" /></div>
                  <div id="carousel-right"><img src="images/awards/arrow-right.png" /></div>
                </div>
              
        </div>
    </section>

    

    

    <?php include_once('includes/footer.php'); ?>


    <!-- featuterd carousel jquery -->
    <script src="js/jquery-1.7.min.js"></script>
    <script src="js/jquery.featureCarousel.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        var carousel = $("#carousel").featureCarousel({
          // include options like this:
          // (use quotes only for string values, and no trailing comma after last option)
          // option: value,
          // option: value
        });

        $("#but_prev").click(function () {
          carousel.prev();
        });
        $("#but_pause").click(function () {
          carousel.pause();
        });
        $("#but_start").click(function () {
          carousel.start();
        });
        $("#but_next").click(function () {
          carousel.next();
        });
      });
    </script>
</body>
</html>